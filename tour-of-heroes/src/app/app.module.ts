import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';


import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
import { AboutComponent } from './about/about.component';

import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { DataService } from './services/data.service';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { MessagesComponent } from './messages/messages.component';


const appRoutes: Routes = [
	{path:'', component: HeroesComponent},
	{path:'about', component: AboutComponent}
];

@NgModule({
	declarations: [
		AppComponent,
		HeroesComponent,
		AboutComponent,
		HeroDetailComponent,
		MessagesComponent
	],
	//@NgModule metadata's imports array
	imports: [
		BrowserModule,
		HttpModule,
		FormsModule,
		RouterModule.forRoot(appRoutes)
	],
	providers: [DataService],
	bootstrap: [AppComponent]
})
export class AppModule { }
