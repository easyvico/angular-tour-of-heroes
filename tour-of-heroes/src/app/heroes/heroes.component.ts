import { HeroService } from './../services/hero.service';
import { DataService } from './../services/data.service';
import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';


@Component({
	selector: 'app-heroes',
	templateUrl: './heroes.component.html',
	styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

	title;
	//age: number;
	//firstName: string;
	//email: string;
	//address: Address;
	//declare un array
	//hobbies: string[];
	//posts: Post[];
	//isEdit:boolean = false;
	
	//on importe le tableau et on l'affecte à heroes propriété
	//heroes = HEROES;

	//définition de la prop heroes instance de la classe Hero
	heroesInstance: Hero[];

	//création d'une instance objHero héritant de la class Hero
	// objHero: Hero = {
	// 	id: 1,
	// 	name: 'Windstorm'
	// };

	//on créer une instance (prop) selectedHero de la classe Hero sans donner de valeurs car
	// il n'y a pas de héros sélectionné au démarrage de l'application.
	selectedHero: Hero;
	
	//instance des services pour les appeler dans notre composant présent
	constructor(private dataService: DataService,
		private heroService: HeroService) {}

	//Créez une fonction pour récupérer les héros du service.
	getHeroes(): void {
		this.heroService.getHeroes().subscribe(heroes =>
			this.heroesInstance = heroes
		);
		console.log('tableau des heros: ',this.heroesInstance);
	}

	// onSelect() is a HeroesComponent method qui recupere le hero en argument
	//on le stock ensuite dans la prop selectedHero
	onSelect(hero) {
		this.selectedHero = hero;
		console.log('this.selectedHero:' ,this.selectedHero);
	}


	ngOnInit() {
		this.getHeroes();



		// this.firstName= 'Laurent Vicherd';
		// this.age= 45;
		// this.email= 'easyvico@hotmail.com';

		// this.address = {
		// 	street: 'Rue de la commanderie',
		// 	city: 'Saint-Vivien',
		// 	state: 'Charente-maritime'
		// }
		// this.hobbies = ['Aikido', 'Astronomie', 'Bande dessinée'];

		// //Appel de la method getPosts pour avoir en retour les infos du json du dataService
		// this.dataService.getPosts().subscribe((posts) => {
		// 	this.posts = posts;
		// 	console.log('Tableau:' ,this.posts);
		// });
	}


	// onClick() {
	// 	console.log('Hello du click');
	// 	this.hobbies.push('Nouvelle passion');
	// 	this.firstName = 'Paul Memphis';
	// }

	// addHobby(hobby) {
	// 	console.log(hobby);
	// 	this.hobbies.unshift(hobby);
	// 	return false;
	// }

	// deleteHobby(i) {
	// 	console.log('delete');
	// 	//on supprime le hobbies du tableau
	// 	this.hobbies.splice(i, 1);
	// }

	// toggleEdit() {
	// 	this.isEdit = !this.isEdit;
	// }

}

//detail de la propriété objet adress déclarée plus haut dans le component
// interface Address {
// 	street: string;
// 	city: string;
// 	state: string;
// }

// interface Post {
// 	title: string;
// 	rating: string;
// 	id: number;
// }