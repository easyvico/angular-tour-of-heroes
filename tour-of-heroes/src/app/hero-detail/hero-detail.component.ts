import { Hero } from './../hero';
import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-hero-detail',
	templateUrl: './hero-detail.component.html',
	styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
	//propriété input de hero qui va faire le lien avec le parent [hero] HeroesComponent 
	@Input() hero: Hero;

	constructor() { }

	ngOnInit() {
	}
	// C'est le seul changement à apporter à la classe HeroDetailComponent. Il n'y a plus de propriétés. Il n'y a pas de logique de présentation. Ce composant reçoit simplement un objet hero via sa propriété hero et l'affiche.
}
