import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogModifManagerComponent } from './dialog-modifier-manager.component';

describe('DialogModifManagerComponentt', () => {
  let component: DialogModifManagerComponent;
  let fixture: ComponentFixture<DialogModifManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogModifManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogModifManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
