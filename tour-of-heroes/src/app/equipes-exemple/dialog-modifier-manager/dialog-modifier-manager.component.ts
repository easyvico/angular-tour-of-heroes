import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-dialog-modifier-manager',
  templateUrl: './dialog-modifier-manager.component.html',
  styleUrls: ['./dialog-modifier-manager.component.scss']
})
export class DialogModifManagerComponent implements OnInit {
  couleurFondSurvol: String = '#E0E8F3';
  couleurTextSurvol: String = '#000';
  texteAucuneDonnee: String = 'Aucune donnée';
  // equipiers: Array<any> = [];
  // equipierSelectionne: Array<string> = [];

  constructor() { }

  ngOnInit() { }

}
