import { Component, OnInit } from '@angular/core';
import { DataService } from '.././services/data.service';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { Hero } from '../hero';
import { Pouvoir } from './../pouvoir';


@Component({
	selector: 'app-about',
	templateUrl: './about.component.html',
	styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
	cars:string[];
	mesDonnees: string = '';
	//prop pour afficher ou pas le power1
	test: boolean;

	power1: Pouvoir = {
		id: 1,
		strenght: 5,
		stanima: 7,
		attitude: 'agressive'
	}

	constructor(private dataService: DataService) {
	}

	ngOnInit() {
		console.log('power1 :' ,this.power1);

		this.cars = this.dataService.car;
		console.log('Car tab :', this.cars);

		this.mesDonnees = this.dataService.myData();
	}

	toggle() {
		this.test = !this.test;
	}

}
