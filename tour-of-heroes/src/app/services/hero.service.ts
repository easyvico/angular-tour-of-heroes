import { Injectable, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HEROES } from '../mock-heroes';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
	providedIn: 'root'
})
export class HeroService {

	constructor(private messageService: MessageService) { }

	//ajout d'une methode provenant de la classe Hero et retournant le tableau HEROES de mock heroes
	//renvoie un observable <Hero []> qui émet une valeur unique, le tableau des héros factices.

  getHeroes(): Observable<Hero[]> {
    // TODO: send the message _after_ fetching the heroes
    this.messageService.add('HeroService: fetched heroes');
    return of(HEROES);
  }
}
