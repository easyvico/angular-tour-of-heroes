import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class DataService {

	constructor(public http: Http) {
		console.log('Data service connected');
	}

	car = [
		'Citroen', 'Ford', 'Peugeot'
	];

	myData() {
		return 'Eh voici mes données !! ouiiiii';
	}

	getPosts() {
		return this.http.get('http://localhost:3000/bookData')
			.pipe(map(response => response.json()));
	}
}
